# Desafio

Repositório com a solução de um desafio para uma vaga de Desenvolvedor.

A API deste desafio pode ser testada tanto com interface gráfica web (pelo navegador) quanto CLI.


#### Dependências

- Python 3
- Flask
- sqlalchemy

#### Como executar

Com Dockerização:

>docker-compose up

>Acessar o endereço http://0.0.0.0:5001/


Sem Dockerização:

>export FLASK_APP=app.py

>flask run

>Acessar o endereço http://0.0.0.0:5000/
