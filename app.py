# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, url_for, flash, redirect, session, jsonify, make_response
import json
import re
import requests
from sqlalchemy import *


db = create_engine('sqlite:///banco.db')
db.echo = False
metadata = MetaData(db)

app = Flask(__name__)


usouweb = False



#Tabelas que serão usadas pelo sqlalchemy
estabelecimento = Table('estabelecimento', metadata,
    Column('nome', String),
    Column('cnpj', String, primary_key=True),
    Column('dono', Integer),
    Column('telefone', Integer),
)


recebimentos = Table('recebimentos', metadata,
    Column('cliente', String),
    Column('valor', Float),
    Column('descricao', String),
    Column('cnpj', String),
)


@app.route('/')
def index():
    return render_template('main.html')

@app.route('/segundodesafio')
def segundodesafio():
    return render_template('segundodesafio.html')


@app.route('/sobremim')
def sobremim():
    return render_template('sobremim.html')


@app.route('/api/v1/transacao', methods=('GET', 'POST'))
def transacao():
    if request.method == 'POST':
        #Recebe o JSON presente no POST:
        dados0 = request.get_json()
        print(dados0)
        #verificando se foi inicializado na web...
        global usouweb
        if(usouweb==False):
            print("Aparentemente o usuário está em CLI...")
        #certificando que o json poderá ser carregando tanto pela versão web como CLI    
        try:
            dados = json.loads(dados0)
        except:
            dados = json.loads(json.dumps(dados0))
        #inicializa a verificação dos dados; A princípio, assume que tudo está correto...
        booleano = True
        x = re.search("[0-9]{2}[\.][0-9]{3}[\.][0-9]{3}[\/][0-9]{4}[-][0-9]{2}", dados["estabelecimento"])
        if x is None:
            #... Até algo não estar correto.
            booleano = False
        #nem deve continuar se antes não encontrou o resultado
        if(booleano!=False):
            x = re.search("[0-9]{3}[\.][0-9]{3}[\.][0-9]{3}[-][0-9]{2}", dados["cliente"])
            if x is None:
                booleano = False
        if(booleano!=False):
            x = re.search("^\d+(?:\.\d{0,2})?$", dados["valor"])
            if x is None:
                booleano = False
        if booleano==True:
            #se todos os dados estiverem em conformidade, retornar true
            resposta = {"aceito": "true"}
            #se o usuário assinalou o checkbox de introduzir dados no banco...
            vercheckbox = False
            try:
                print(dados["chkbox"]=="on")
                vercheckbox = True
            except:
                vercheckbox = False
            if (vercheckbox==True) or (usouweb==False):
                i = recebimentos.insert()
                i.execute(cliente=dados["cliente"], valor=dados["valor"], descricao=dados["descricao"], cnpj=dados["estabelecimento"])
            else:
                print("Não irá salvar os dados no banco...")
            #retorna o objeto json
            return resposta
        else:
            resposta = {"aceito": "false"}
            #retorna o objeto json      
            return resposta


@app.route('/api/v1/', methods=('GET', 'POST'))
def transacaoview():
    if request.method == 'GET':
        global usouweb
        usouweb=True
        return render_template('transacao.html')
    if request.method == 'POST':
        #coleta os resultados como json:
        objson = request.form.to_dict()
        app_json = json.dumps(objson)
        #Envia para a rota designada, que deve fazer a verificação dos dados
        response = requests.post(request.url_root + url_for("transacao"),json=app_json)
        #Retorna o resultado da rota:
        return (response.json())
    




#esta rota foi criada apenas para a view onde será introduzido o CNPJ das empresas.
@app.route('/api/v1/transacoes/', methods=('GET', 'POST'))
def transacoes():
    s = estabelecimento.select()
    rs = s.execute()
    estabelecimentos = rs.fetchall()
    if request.method == 'POST':
        objson = jsonify(request.form)
        dados = objson.json
        x = re.search("[0-9]{2}[\.][0-9]{3}[\.][0-9]{3}[\/][0-9]{4}[-][0-9]{2}", dados["estabelecimento"])
        if x is None:
            return redirect(url_for('transacoes'))
        else:
            return redirect(url_for('listing')+ "?cnpj=" +dados["estabelecimento"])
    return render_template('transacoes.html',estabelecimentos=estabelecimentos)


#Endpoint em GET que recebe o CPNJ, e retorna a lista de transações
@app.route('/api/v1/transacoes/estabelecimento', methods=('GET', 'POST'))
def listing():
    cnpj = request.args.get('cnpj', None)
    s = select([estabelecimento]).where(estabelecimento.columns.cnpj==cnpj)
    rs = s.execute()

    row = rs.fetchone()
    if(row!=None):
        data = {
        "estabelecimento" : {

        "nome": row.nome,
        'cnpj': row.cnpj,
        'dono': row.dono,
        'telefone': row.telefone
        }
    }

        s = select([recebimentos]).where(recebimentos.columns.cnpj==cnpj)
        rs = s.execute()

        rows = rs.fetchall()
        
        data2=[]
        totalrecebido=0
        for x in rows:
            totalrecebido=totalrecebido+x.valor
            data3 = {
        "cliente": x.cliente,
        'descricao': x.descricao,
        'valor': x.valor
        }
            data2.append(data3)
        data3={
            "recebimentos":data2
        }

        data.update(data3)

        data3={
            "total_recebido":totalrecebido
        }

        data.update(data3)

        jsonified_data = json.dumps(data)
        return jsonify(data)
    else:
        return "Não foi possível encontrar resultados para este CNPJ."
    
    
if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
